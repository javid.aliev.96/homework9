package Controller;

import Service.FamilyService;
import Entity.*;

import java.util.List;
import java.util.Set;


public class FamilyController {

    private FamilyService familyService = new FamilyService();

    public int count() {
        return this.familyService.count();
    }

    public Family getFamilyByIndex(int index) {
        return this.familyService.getFamilyByIndex(index);
    }

    public void saveFamily(Family family) {
        familyService.saveFamily(family);
    }

    public List<Family> getAllFamilies() {
        return familyService.getAllFamilies();
    }

    public List<Family> displayAllFamilies() {
        return familyService.displayAllFamilies();
    }

    public List<Family> getFamiliesBiggerThan(int number) {
        return familyService.getFamiliesBiggerThan(number);
    }

    public List<Family> getFamiliesLessThan(int number) {
        return familyService.getFamiliesLessThan(number);
    }

    public List<Family> countFamiliesWithMemberNumber(int number) {
        return familyService.countFamiliesWithMemberNumber(number);
    }

    public boolean deleteFamilyByIndex(int index) {
        return familyService.deleteFamilyByIndex(index);
    }

    public boolean deleteFamilyByFamily(Family family) {
        return familyService.deleteFamilyByFamily(family);
    }

    public boolean createNewFamily(Human mother, Human father) {
        return familyService.createNewFamily(mother, father);
    }

    public Family bornChild(Family family, String boyName, String girlName) {
        return familyService.bornChild(family, boyName, girlName);
    }

    public Family adoptChild(Family family, Human adoptedChild) {
        return familyService.adoptChild(family, adoptedChild);
    }

    public void deleteAllChildrenOlderThan(int number) {
        familyService.deleteAllChildrenOlderThan(number);
    }

    public Family getFamilyById(int id) {
        return familyService.getFamilyById(id);
    }

    public Set<Pet> getPets(int index) {
        return familyService.getPets(index);
    }

    public void addPet(int index, Pet pet) {
        familyService.addPet(index, pet);
    }
}
