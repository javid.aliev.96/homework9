package Service;

import DAO.CollectionFamilyDao;
import DAO.FamilyDao;
import Entity.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;


public class FamilyService { // CONTAINS ALL LOGIC, IF IS POSSIBLE THEN PROCEEDS TO DAO AND FURTHER, IF NOT RETURNS BACK TO CONTROLLER

    private final FamilyDao familyDao = new CollectionFamilyDao();

    public int count() {
        return familyDao.count();
    }

    public List<Family> getAllFamilies() {

        return familyDao.getAllFamilies();
    }

    public List<Family> displayAllFamilies() {
        List<Family> families = getAllFamilies();
        for (Family family : families) {
            System.out.println(family.toString());
        }
        return families;
    }


    public List<Family> getFamiliesBiggerThan(int number) {
        List<Family> familyList = new ArrayList<>();
        for (int i = 0; i < familyDao.count(); i++) {
            if (familyDao.getFamilyByIndex(i).countFamily() > number) {
                familyList.add(familyDao.getFamilyByIndex(i));
                System.out.println(familyDao.getFamilyByIndex(i).toString());
            }
        }
        return familyList;
    }

    public List<Family> getFamiliesLessThan(int number) {
        List<Family> familyList = new ArrayList<>();
        for (int i = 0; i < familyDao.count(); i++) {
            if (familyDao.getFamilyByIndex(i).countFamily() < number) {
                familyList.add(familyDao.getFamilyByIndex(i));
                System.out.println(familyDao.getFamilyByIndex(i).toString());
            }
        }
        return familyList;
    }

    public List<Family> countFamiliesWithMemberNumber(int number) {
        List<Family> familyList = new ArrayList<>();
        for (int i = 0; i < familyDao.count(); i++) {
            if (familyDao.getFamilyByIndex(i).countFamily() == number) {
                familyList.add(familyDao.getFamilyByIndex(i));
                System.out.println(familyDao.getFamilyByIndex(i).toString());
            }
        }
        return familyList;
    }

    public Family getFamilyByIndex(int index) {
        try {
            System.out.println(familyDao.getFamilyByIndex(index));
            return familyDao.getFamilyByIndex(index);
        } catch (Exception e) {
            System.out.println("null");
            return null;
        }
    }

    public boolean deleteFamilyByIndex(int index) {
        boolean deleted;
        try {
            familyDao.deleteFamily(index);
            System.out.println("true");
            deleted = true;
        } catch (Exception e) {
            System.out.println("false");
            deleted = false;
        }
        return deleted;
    }

    public boolean deleteFamilyByFamily(Family family) {
        boolean deleted;
        try {
            if (familyDao.getAllFamilies().contains(family)) {
                familyDao.deleteFamily(family);
                System.out.println("true");
                deleted = true;
            } else {
                System.out.println("false");
                deleted = false;
            }
        } catch (Exception e) {
            System.out.println("false");
            deleted = false;
        }
        return deleted;
    }

    public void saveFamily(Family family) {
        if (familyDao.getAllFamilies().contains(family)) {
            int permanentIndex = familyDao.getFamilyIndex(family);
            familyDao.deleteFamily(familyDao.getFamilyIndex(family));
            familyDao.saveFamilyByIndex(permanentIndex, family);
        } else
            familyDao.saveFamily(family);
    }

    public boolean createNewFamily(Human mother, Human father) {
        return familyDao.saveFamily(new Family(mother, father));
    }

    public Family bornChild(Family family, String boyName, String girlName) {
        int index = familyDao.getFamilyIndex(family);
        if (boyName != null && girlName == null) {
            familyDao.getFamilyByIndex(index).addChild(new Man(boyName, familyDao.getFamilyByIndex(index).getFather().getSurname(), 10, family.getPet()));
        } else if (boyName == null && girlName != null) {
            familyDao.getFamilyByIndex(index).addChild(new Woman(girlName, familyDao.getFamilyByIndex(index).getFather().getSurname(), 18, family.getPet()));
        } else if (boyName != null && girlName != null) {
            familyDao.getFamilyByIndex(index).addChild(new Man(boyName, familyDao.getFamilyByIndex(index).getFather().getSurname(), 10, family.getPet()));
            familyDao.getFamilyByIndex(index).addChild(new Woman(girlName, familyDao.getFamilyByIndex(index).getFather().getSurname(), 18, family.getPet()));
        } else {
            System.out.println("Incorrect Input Data, Child/Children was/were not born!");
        }
        return familyDao.getFamilyByIndex(index);
    }

    public Family adoptChild(Family family, Human adoptedChild) {
        int index = familyDao.getFamilyIndex(family);
        familyDao.getFamilyByIndex(index).addChild(adoptedChild);
        return familyDao.getFamilyByIndex(index);
    }

    public void deleteAllChildrenOlderThan(int number) {
        for (int i = 0; i < familyDao.count(); i++) {
            for (int j = 0; j < familyDao.getFamilyByIndex(i).getChildrenList().size(); j++) {
                if (familyDao.getFamilyByIndex(i).getChildAge(j) > number) {
                    familyDao.getFamilyByIndex(i).getChildrenList().set(j, null);
                }
            }
        }
    }

    public Family getFamilyById(int id) {
        try {
            System.out.println(familyDao.getFamilyByIndex(id));
            return familyDao.getFamilyByIndex(id);
        } catch (Exception e) {
            System.out.println("No matches found.");
            return null;
        }
    }

    public Set<Pet> getPets(int index) {
        try {
            return familyDao.getFamilyByIndex(index).getPetList();
        } catch (Exception e) {
            System.out.println("No matches found.");
            return null;
        }
    }

    public void addPet(int index, Pet pet) {
        try {
            familyDao.getFamilyByIndex(index).setPet(pet);
        } catch (Exception e) {
            System.out.println("No matches found.");
        }
    }

}

