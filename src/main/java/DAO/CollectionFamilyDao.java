package DAO;

import Entity.*;

import java.util.ArrayList;
import java.util.List;

public class CollectionFamilyDao implements FamilyDao {

    private final List<Family> familyList;

    public CollectionFamilyDao() {
        familyList = new ArrayList<>();
    }

    @Override
    public List<Family> getAllFamilies() {
        return familyList;
    }

    @Override
    public Family getFamilyByIndex(int index) {
        return this.familyList.get(index);
    }

    @Override
    public void deleteFamily(int index) {
        this.familyList.remove(index);
    }

    @Override
    public void deleteFamily(Family family) {
        this.familyList.remove(family);
    }

    @Override
    public boolean saveFamily(Family family) {
        return this.familyList.add(family);
    }

    @Override
    public int getFamilyIndex(Family family) {
        return this.familyList.indexOf(family);
    }

    @Override
    public void saveFamilyByIndex(int index, Family family) {
        this.familyList.add(index, family);
    }

    public int count() {
        return this.familyList.size();
    }

}

