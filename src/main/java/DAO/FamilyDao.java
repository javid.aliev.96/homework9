package DAO;

import Entity.*;

import java.util.List;

public interface FamilyDao {

    List<Family> getAllFamilies();

    Family getFamilyByIndex(int index);

    void deleteFamily(int index);

    void deleteFamily(Family family);

    boolean saveFamily(Family family);

    int getFamilyIndex(Family family);

    void saveFamilyByIndex(int index, Family family);

    int count();

}
