import Controller.FamilyController;
import Entity.*;

public class Main {

    private final static FamilyController familyController = new FamilyController();

    public static void main(String[] args) {

        Human test1 = new Human("Mother", "Test1", 50);
        Human test2 = new Human("Father", "Test1", 55);
        Dog rex = new Dog("Rex", 5, 55, new String[]{"eat, sleep, run"});
        DomesticCat garfield = new DomesticCat("Garfield", 3, 99, new String[]{"trick Rex, take a nap, play with curtains"});
        Man childSon = new Man("Son", "Test1", 25, rex);
        Family testFamily = new Family(0, "!!");
        testFamily.setFather(test2);
        testFamily.setMother(test1);
        testFamily.setPet(rex);

        familyController.saveFamily(testFamily);                                                               // saveFamily(Entity.Family family) method

        familyController.adoptChild(testFamily, childSon);                                                     // adoptChild(Entity.Family family, Entity.Human adoptedChild) method

        familyController.bornChild(testFamily, "Mamed", null);                                // bornChild(Entity.Family family, String boyName, String girlName) method

        familyController.addPet(0, garfield);                                                            // addPet(int index, Entity.Pet pet) method

        familyController.getPets(0);                                                                     // getPets(int index) method

        System.out.println();
        System.out.println();

        Human test3 = new Human("Mother", "SampleFamily", 54);
        Human test4 = new Human("Father", "SampleFamily", 59);
        Family sampleFamily = new Family(1, "SampleName");
        sampleFamily.setMother(test3);
        sampleFamily.setFather(test4);
        familyController.saveFamily(sampleFamily);

        System.out.println(familyController.count());                                                          // count() method
        System.out.println();
        System.out.println(familyController.getAllFamilies());                                                 // getAllFamilies() method
        System.out.println();
        familyController.displayAllFamilies();                                                                 // displayAllFamilies() method
        System.out.println();
        familyController.getFamiliesBiggerThan(3);                                                      // getFamiliesBiggerThan(int number) method - HINT: displays "testFamily" family
        System.out.println();
        familyController.getFamiliesLessThan(3);                                                        // getFamiliesLessThan(int number) method - HINT: displays "sampleFamily" family
        System.out.println();
        familyController.countFamiliesWithMemberNumber(2);                                                     // countFamiliesWithMemberNUmber(int number) method - HINT: displays "SampleFamily" family
        System.out.println();
        familyController.getFamilyByIndex(0);                                                                  // getFamilyByIndex(int index) method - HINT: displays "testFamily" family
        System.out.println();
        familyController.deleteFamilyByIndex(0);                                                               // deleteMethodByIndex(int index) method - HINT: deletes "testFamily" family
        System.out.println();
        familyController.getFamilyById(3);                                                                     // getFamilyById(int index) method
        System.out.println();
        familyController.deleteFamilyByFamily(sampleFamily);                                                   // deleteMethodByFamily(Entity.Family family) method - HINT: deletes "sampleFamily" family
        System.out.println();

        Human testFather = new Human("For ", "Test", 40);
        Human testMother = new Human("For ", "Test", 35);

        System.out.println(familyController.count());                                                          // verifying that all families are erased
        familyController.createNewFamily(testMother, testFather);                                              // createNewFamily(Entity.Human mother, Entity.Human father) method
        System.out.println(familyController.count());                                                          // verifying that createNewFamily method implements created family to list

    }
}






