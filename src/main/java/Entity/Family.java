package Entity;

import DAO.CollectionFamilyDao;

import java.util.*;

public class Family extends CollectionFamilyDao {
    private int id;
    private String name;
    private Human mother;
    private Human father;
    private Pet pet;
    private Set<Pet> petList = new HashSet<>();
    private List<Human> childrenList = new ArrayList();

    public Family(Human mother, Human father) {
        setMother(mother);
        setFather(father);
        this.petList = new HashSet<>();
    }

    public Family(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Human getMother() {

        return mother;
    }

    public void setMother(Human mother) {

        this.mother = mother;
    }

    public Human getFather() {

        return father;
    }

    public void setFather(Human father) {

        this.father = father;
    }

    public Pet getPet() {

        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
        this.petList.add(pet);
    }

    public Set<Pet> getPetList() {
        System.out.println("Pets of the family are: " + petList.toString());
        return petList;
    }


    public List getChildrenList() {
        return childrenList;
    }

    public void setChildrenList(List childrenList) {
        this.childrenList = childrenList;
    }

    public int getChildAge(int index) {
        return this.childrenList.get(index).getAge();
    }

    public void addChild(Human human) {

        this.childrenList.add(human);
    }

    public boolean deleteChild(int index) {
        boolean deletion;
        try {
            this.childrenList.remove(index);
            deletion = true;
        } catch (Exception exception) {
            System.out.println("Your required child is not correct");
            deletion = false;
        }
        return deletion;
    }

    public boolean deleteChild(Human human) {
        boolean deletion = false;
        if (this.childrenList.contains(human)) {
            this.childrenList.remove(human);
            return deletion = true;
        } else {
            return deletion = false;
        }
    }

    public int countFamily() {

        return this.childrenList.size() + 2;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Family)) return false;
        Family family = (Family) o;
        return Objects.equals(getMother(), family.getMother()) && Objects.equals(getFather(), family.getFather()) && Objects.equals(getPet(), family.getPet()) && Objects.equals(childrenList, family.childrenList);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(getMother(), getFather(), getPet());
        result = 31 * result + Objects.hashCode(childrenList);
        return result;
    }

    @Override
    public String toString() {
        return "Entity.Family{" +
                "mother=" + mother +
                ", father=" + father +
                ", pet=" + pet +
                ", childrenList=" + childrenList +
                '}';
    }
}
