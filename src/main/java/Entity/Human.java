package Entity;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;

public class Human {
    private String name;
    private String surname;
    private int age;
    private int iq;
    public Map schedule = new LinkedHashMap();
    private Family family;

    public Human(String name, String surname, int age, int iq) {
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.iq = iq;
    }

    public Human(String name, String surname, int age) {
        this.name = name;
        this.surname = surname;
        this.age = age;
    }

    public Human() {
    }

    public void scheduleGetter() {
        System.out.println("The schedule is: " + this.schedule.toString());
    }

    public void scheduleSetter(String dayOfWeek, String tasks) {
        this.schedule.put(dayOfWeek, tasks);
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {

        this.name = name;
    }

    public String getSurname() {

        return surname;
    }

    public void setSurname(String surname) {

        this.surname = surname;
    }

    public int getAge() {

        return age;
    }

    public void setAge(int age) {

        this.age = age;
    }

    public int getIq() {

        return iq;
    }

    public void setIq(int iq) {

        this.iq = iq;
    }

    public Map<DayOfWeek, String> getSchedule() {

        return schedule;
    }

    public void setSchedule(Map<DayOfWeek, String> schedule) {

        this.schedule = schedule;
    }

    public Family getFamily() {

        return family;
    }

    public void setFamily(Family family) {

        this.family = family;
    }

    public void greetPet() {
        System.out.println("Hello, " + family.getPet().getNickname());
    }

    public void describePet() {
        if (family.getPet().getTrickLevel() > 50)
            System.out.println("I have a " + family.getPet().getSpecies() + ", he is " + family.getPet().getAgeOfPet() + " years old, he is very sly.");
        else {
            System.out.println("I have a " + family.getPet().getSpecies() + ", he is " + family.getPet().getAgeOfPet() + " years old, he is almost not sly.");
        }
    }

    public boolean feedPet(boolean time) {
        boolean feeding;
        Random random = new Random(101);
        if (time == true) {
            feeding = true;
        } else if (family.getPet().getTrickLevel() >= random.nextInt()) {
            System.out.println("Hm...I will feed " + family.getPet().getNickname());
            feeding = true;
        } else {
            System.out.println("I think " + family.getPet().getNickname() + " is not hungry.");
            feeding = false;
        }
        return feeding;
    }

    @Override
    public String toString() {
        return "Entity.Human{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", age=" + age +
                ", iq=" + iq +
                ", schedule=" + schedule.toString() +
                '}';
    }
}
