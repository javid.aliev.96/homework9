package Entity;

public final class Woman extends Human {
    private Pet pet;

    public Woman(String name, String surname, int age, Pet pet) {
        super(name, surname, age);
        this.pet = pet;
    }

    @Override
    public void scheduleSetter(String dayOfWeek, String tasks) {
        super.scheduleSetter(dayOfWeek, tasks);
    }

    @Override
    public void scheduleGetter() {
        super.scheduleGetter();
    }

    public void whine() {
        System.out.println("We need to talk");
    }


    @Override
    public void greetPet() {
        System.out.println("Hello my dear " + this.pet.getNickname() + " I missed you a lot!");
    }
}
