import Entity.*;
import Service.FamilyService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FamilyServiceTest {
    public Family testfamily;
    private Human testch1;
    private Human testch2;
    DomesticCat garfield;
    Dog rex;
    FamilyService familyService = new FamilyService();

    @BeforeEach
    void testInfo() {
        Human father = new Human("Father", "Test1", 55);
        Human mother = new Human("Mother", "Test1", 50);
        testfamily = new Family(0, "TestFamily");
        rex = new Dog("Rex", 5, 55, new String[]{"eat, sleep, run"});
        testch1 = new Man("Son", "Test1", 25, rex);
        garfield = new DomesticCat("Garfield", 3, 99, new String[]{"trick Rex, take a nap, play with curtains"});
        testch2 = new Woman("Daughter", "Test1", 24, garfield);
        testfamily.setMother(mother);
        testfamily.setFather(father);
        testfamily.setPet(rex);
        familyService.saveFamily(testfamily);

        testch1 = new Human("Test", "Child1", 25);
        testch2 = new Human("Test", "Child2", 17);
    }

    @Test
    void count() {
        assertEquals(1, familyService.count());
    }

    @Test
    void getAllFamilies() {
        assertTrue(familyService.getAllFamilies().contains(testfamily));
    }

    @Test
    void displayAllFamilies() {
        assertTrue(familyService.displayAllFamilies().contains(testfamily));
    }

    @Test
    void getFamiliesBiggerThan() {
        assertTrue(familyService.getFamiliesBiggerThan(1).contains(testfamily));
    }

    @Test
    void getFamiliesLessThan() {
        assertTrue(familyService.getFamiliesLessThan(3).contains(testfamily));
    }

    @Test
    void countFamiliesWithMemberNumber() {
        assertTrue(familyService.countFamiliesWithMemberNumber(2).contains(testfamily));
    }

    @Test
    void getFamilyByIndex() {
        assertEquals(testfamily, familyService.getFamilyByIndex(0));
    }

    @Test
    void deleteFamilyByIndex() {
        assertTrue(familyService.deleteFamilyByIndex(0));
    }

    @Test
    void deleteFamilyByFamily() {
        assertTrue(familyService.deleteFamilyByFamily(testfamily));
    }

    @Test
    void saveFamily() {
        Human test3 = new Human("Mother", "SampleFamily", 54);
        Human test4 = new Human("Father", "SampleFamily", 59);
        Family sampleFamily = new Family(1, "SampleName");
        sampleFamily.setFather(test4);
        sampleFamily.setMother(test3);

        assertFalse(familyService.getAllFamilies().contains(sampleFamily)); // before saving family

        familyService.saveFamily(sampleFamily);

        assertTrue(familyService.getAllFamilies().contains(sampleFamily)); // after saving family
    }

    @Test
    void createNewFamily() {
        Human test3 = new Human("Mother", "SampleFamily", 54);
        Human test4 = new Human("Father", "SampleFamily", 59);

        assertTrue(familyService.createNewFamily(test3, test4));

    }

    @Test
    void bornChild() {
        assertEquals(2, testfamily.countFamily()); // before children were born
        familyService.bornChild(testfamily, "Mamed", "Pakize");
        assertEquals(4, testfamily.countFamily()); // after children were born
    }

    @Test
    void adoptChild() {
        assertEquals(2, testfamily.countFamily()); // before child was adopted
        familyService.adoptChild(testfamily, testch1);
        assertEquals(3, testfamily.countFamily()); // after child was adopted
    }

    @Test
    void deleteAllChildrenOlderThan() {
        familyService.adoptChild(testfamily, testch1); // age 25
        familyService.adoptChild(testfamily, testch2); // age 17

        familyService.deleteAllChildrenOlderThan(19);

        assertFalse(testfamily.getChildrenList().contains(testch1)); // does not exist anymore
        assertTrue(testfamily.getChildrenList().contains(testch2));  // exists

    }

    @Test
    void getFamilyById() {
        String expected = "Entity.Family{mother=Entity.Human{name='Mother', surname='Test1', age=50, iq=0, schedule={}}, " +
                "father=Entity.Human{name='Father', surname='Test1', age=55, iq=0, schedule={}}, " +
                "pet=Dog{nickname='Rex, age=5, trickLevel=55, habits=[eat, sleep, run]}, childrenList=[]}";

        assertEquals(expected, familyService.getFamilyById(0).toString());
    }

    @Test
    void getPets() {
        assertTrue(familyService.getPets(0).contains(rex));
    }

    @Test
    void addPet() {
        familyService.addPet(0, garfield);
        assertTrue(familyService.getPets(0).contains(garfield));
    }
}